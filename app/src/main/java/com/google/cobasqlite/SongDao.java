package com.google.cobasqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class SongDao {

    public static String TABLE_MS_SONG = "msSong";
    public static String FIELD_MS_SONG_TITLE = "songTitle";
    public static String FIELD_MS_SONG_ARTIST = "songArtist";

    public void createTableSong(SQLiteDatabase db){
        String qCreate = "CREATE TABLE IF NOT EXISTS '" + TABLE_MS_SONG + "'(\n"+
                "\t'"+FIELD_MS_SONG_TITLE+"' TEXT,\n"+
                "\t'"+FIELD_MS_SONG_ARTIST+"' TEXT);";
        db.execSQL(qCreate);
    }

    public void insertSong(Context mCtx, Song song){
        MySqliteHelper helper = new MySqliteHelper(mCtx);
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(FIELD_MS_SONG_TITLE, song.songTitle);
        cv.put(FIELD_MS_SONG_ARTIST, song.songArtist);
        db.insertWithOnConflict(
                TABLE_MS_SONG,
                null,
                cv,
                SQLiteDatabase.CONFLICT_REPLACE
        );
        db.close();
    }

    public void insertSongs(Context mCtx ,ArrayList<Song> songs){
        for(int i=0;i<songs.size();i++){
            insertSong(mCtx,songs.get(i));
        }
    }

    public ArrayList<Song> getAllSongs (Context mCtx){
        ArrayList<Song> songsResult = new ArrayList<>();

        MySqliteHelper helper = new MySqliteHelper(mCtx);
        SQLiteDatabase db = helper.getReadableDatabase();

        Cursor resultCursor = db.query(
                TABLE_MS_SONG,
                null,null,null,
                null,null,null);

        while(resultCursor.moveToNext()){
            String songTitle = resultCursor.getString(0); //0 adalah index untuk kolom pertama
            String songArtist = resultCursor.getString(1); //1 adalah index untuk kolom kedua
            songsResult.add(new Song(songTitle,songArtist)); //simpan hasil dari db ke array list
        };
        return songsResult;
    }


}
