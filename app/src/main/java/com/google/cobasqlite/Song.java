package com.google.cobasqlite;

public class Song {

    public String songTitle;
    public String songArtist;

    public Song(String songTitle, String songArtist){
        this.songTitle = songTitle;
        this.songArtist = songArtist;
    }

}
