package com.google.cobasqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MySqliteHelper extends SQLiteOpenHelper {

    public static final String dbName = "dbMusic";
    public static final int dbVersion = 2;
    public static final SQLiteDatabase.CursorFactory cFactory = null;

    public MySqliteHelper(Context context){
        super(context, dbName, cFactory, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //hanya dipanggil sekali ketika db pertama dibuat.
        //perlu create table-table yg dibutuhkan dari awal

        SongDao songDao = new SongDao();
        songDao.createTableSong(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
