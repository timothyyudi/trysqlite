package com.google.cobasqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Song lagu1 = new Song("judul1","artist1");
        Song lagu2 = new Song("judul2","artist2");
        ArrayList<Song> semuaLagu = new ArrayList<Song>();
        semuaLagu.add(lagu1);
        semuaLagu.add(lagu2);

        SongDao songDao = new SongDao();
        songDao.insertSongs(this,semuaLagu);

        Log.v("hasil",songDao.getAllSongs(this).get(1).songArtist);

    }
}